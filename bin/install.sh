#!/bin/bash
clear

echo "INSTALLING COMPOSER PACKAGES"

composer install

echo "DELETING DATABASE"

php bin/console doctrine:database:drop --if-exists --force

echo "CREATING DATABASE"

php bin/console doctrine:database:create --if-not-exists

echo "CREATING DATABASE SCHEMA"

echo "y" | php bin/console doctrine:migrations:migrate

echo "LOADING FIXTURES"

echo "y" | php bin/console doctrine:fixtures:load

echo "TRIGGERING DEVELOPMENT SERVER"

symfony server:start

echo "APPLICATION IS UP AND RUNNING - BROWSE http://127.0.0.1:8000"

<?php

declare(strict_types=1);

namespace App\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class PreferenceExtension extends AbstractExtension
{
    protected $biases;

    protected $subtitles;

    protected $switchedBias;


    public function __construct(array $biases, array $subtitles)
    {
        $this->biases = $biases;

        $this->subtitles = $subtitles;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('switch_bias', [$this, 'getSwitchedBias']),
            new TwigFunction('switch_subtitle', [$this, 'getSwitchedSubtitle']),
            new TwigFunction('subtitle', [$this, 'getSubtitle']),
        ];
    }

    protected function setSwitchedBias(Int $bias): Int
    {
        $biases = $this->biases;
        unset($biases[$bias]);
        $biases = array_values(array_flip($biases));

        $this->switchedBias = $biases[0];

        return $this->switchedBias;
    }

    public function getSwitchedBias(Int $bias): Int
    {
        return $this->switchedBias ?? $this->setSwitchedBias($bias);
    }

    public function getSwitchedSubtitle(Int $bias): string
    {
        if ($this->switchedBias === null) {
            $this->setSwitchedBias($bias);
        }

        return $this->subtitles[$this->switchedBias];
    }

    public function getSubtitle(Int $bias): String
    {
        return $this->subtitles[$bias];
    }
}

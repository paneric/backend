<?php

declare(strict_types=1);

namespace App\Controller;

use App\Action\Person\AddAction;
use App\Action\Person\EditAction;
use App\Action\Person\RemoveAction;
use App\Action\Person\ShowAllAction;
use App\Action\Person\ShowByStateAction;
use Symfony\Component\HttpFoundation\Response;

class PersonController extends AbstractController
{
    public function showAll(ShowAllAction $action): Response
    {
        return $this->render(
            'person/show.html.twig',
            $action->showAll()
        );
    }

    public function showByState(String $state, ShowByStateAction $action): Response
    {
        return $this->render(
            'person/show.html.twig',
            $action->showByState($state)
        );
    }

    public function add(AddAction $action): Response
    {
        $result = $action->add();

        if ($result === null) {
            return $this->redirect('/person/show-all');
        }

        return $this->render(
            'person/add.html.twig',
            $result
        );
    }

    public function edit(String $id, EditAction $action): Response
    {
        $result = $action->edit($id);

        if ($result === null) {
            return $this->redirect('/person/show-all');
        }

        return $this->render(
            'person/edit.html.twig',
            $result
        );
    }

    public function remove(String $id, RemoveAction $action): Response
    {
        $action->remove($id);

        return $this->redirect('/person/show-all');
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Action\PersonProduct\EditPersonProductAction;
use App\Action\PersonProduct\ShowPersonProductsAction;
use App\Action\PersonProduct\ShowProductsByBiasAction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class PersonProductController extends AbstractController
{
    public function showPersonProducts(String $personId, String $bias, ShowPersonProductsAction $action): Response
    {
        return $this->render(
            'person_product/show_person_products.html.twig',
            $action->showPersonProducts($personId, $bias)
        );
    }

    public function editPersonProduct(String $personId, String $productId, String $bias, EditPersonProductAction $action): Response
    {
        $action->editPersonProduct($personId, $productId, $bias);

        return $this->redirect(sprintf(
            '/person/%s/product/show/%s',
            $personId,
            $bias
        ));
    }

    public function showProductsByBias(String $bias, ShowProductsByBiasAction $action): Response
    {
        return $this->render(
            'person_product/show_products_by_bias.html.twig',
            $action->showProductsByBias($bias)
        );
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Action\Product\AddAction;
use App\Action\Product\EditAction;
use App\Action\Product\RemoveAction;
use App\Action\Product\ShowAllAction;
use App\Action\Product\ShowByStateAction;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends AbstractController
{
    public function showAll(ShowAllAction $action): Response
    {
        return $this->render(
            'product/show.html.twig',
            $action->showAll()
        );
    }

    public function showByState(String $state, ShowByStateAction $action): Response
    {
        return $this->render(
            'product/show.html.twig',
            $action->showByState($state)
        );
    }

    public function add(AddAction $action): Response
    {
        $result = $action->add();

        if ($result === null) {
            return $this->redirect('/product/show-all');
        }

        return $this->render(
            'product/add.html.twig',
            $result
        );
    }

    public function edit(String $id, EditAction $action): Response
    {
        $result = $action->edit($id);

        if ($result === null) {
            return $this->redirect('/product/show-all');
        }

        return $this->render(
            'product/edit.html.twig',
            $result
        );
    }

    public function remove(String $id, RemoveAction $action): Response
    {
        $action->remove($id);

        return $this->redirect('/product/show-all');
    }
}

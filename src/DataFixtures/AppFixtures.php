<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Factory\PersonFactory;
use App\Factory\ProductFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        ProductFactory::createMany(20);

        PersonFactory::new()
            ->many(20)
            ->create(function() {
                return ['product' => ProductFactory::randomRange(0, 5)];
            })
        ;
    }
}

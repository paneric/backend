<?php

declare(strict_types=1);

namespace App\Action\PersonProduct;

use App\Query\PersonProductQuery;

class ShowProductsByBiasAction
{
    protected $query;

    public function __construct(PersonProductQuery $query)
    {
        $this->query = $query;
    }

    public function showProductsByBias(String $bias): array
    {
        return [
            'products' => $this->query->findProductsByBias((Int) $bias),
            'bias' => $bias
        ];
    }
}
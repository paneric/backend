<?php

declare(strict_types=1);

namespace App\Action\PersonProduct;

use App\Query\PersonProductQuery;

class ShowPersonProductsAction
{
    protected $query;

    protected $biases;

    public function __construct(PersonProductQuery $query)
    {
        $this->query = $query;
    }

    public function showPersonProducts(String $personId, String $bias): array
    {
        return [
            'products' => $this->query->findPersonProductsByBias((Int)$personId, (Int) $bias),
            'bias' => $bias,
            'person_id' => $personId,
        ];
    }
}

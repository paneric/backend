<?php

declare(strict_types=1);

namespace App\Action\PersonProduct;

use App\Entity\Person;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class EditPersonProductAction
{
    protected $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function editPersonProduct(String $personId, String $productId, String $bias): void
    {
        $person = $this->manager
            ->getRepository(Person::class)
            ->find((Int) $personId);

        $product = $this->manager
            ->getRepository(Product::class)
            ->find((Int) $productId);

        if ($person === null || $product === null) {
            return;
        }

        if ((Int) $bias === 2) {
            $person->removeProduct($product);

            $this->manager->flush();

            return;
        }

        $person->addProduct($product);

        $this->manager->flush();
    }
}

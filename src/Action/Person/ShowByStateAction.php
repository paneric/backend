<?php

declare(strict_types=1);

namespace App\Action\Person;

use App\Repository\PersonRepository;

class ShowByStateAction
{
    protected $repository;

    protected $states;

    public function __construct(PersonRepository $repository, array $states)
    {
        $this->repository = $repository;

        $this->states = $states;
    }

    public function showByState(String $state): array
    {
        return [
            'persons' => $this->repository->findBy(
                ['state' => (Int) $state]
            ),
            'states' => array_flip($this->states),
        ];
    }
}

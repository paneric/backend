<?php

declare(strict_types=1);

namespace App\Action\Person;

use App\Repository\PersonRepository;

class ShowAllAction
{
    protected $repository;

    protected $states;

    public function __construct(PersonRepository $repository, array $states)
    {
        $this->repository = $repository;

        $this->states = $states;
    }

    public function showAll(): array
    {
        return [
            'persons' => $this->repository->findAll(),
            'states' => array_flip($this->states),
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Action\Person;

use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;

class RemoveAction
{
    protected $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function remove(String $id): void
    {
        $person = $this->manager
            ->getRepository(Person::class)
            ->find((Int) $id);

        if ($person !== null) {
            $person->setState(3);

            $this->manager->flush();
        }
    }
}

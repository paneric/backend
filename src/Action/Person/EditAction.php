<?php

declare(strict_types=1);

namespace App\Action\Person;

use App\Entity\Person;
use App\Form\PersonType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface as FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class EditAction
{
    protected $manager;

    protected $formFactory;

    protected $requestStack;

    public function __construct(EntityManagerInterface $manager, FormFactory $formFactory, RequestStack $requestStack)
    {
        $this->manager = $manager;

        $this->formFactory = $formFactory;

        $this->requestStack = $requestStack;
    }

    public function edit(String $id): ?array
    {
        $request = $this->requestStack->getCurrentRequest();

        $person = $this->manager
            ->getRepository(Person::class)
            ->find((Int) $id);

        if ($request !== null && $request->getMethod() === 'POST') {

            $form = $this->formFactory->create(PersonType::class, $person);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->manager->flush();

                return null;
            }
        }

        $form = $this->formFactory->create(PersonType::class, $person);

        return [
            'form' => $form->createView(),
            'id' => $id,
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepository;

class ShowAllAction
{
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function showAll(): array
    {
        return [
            'products' => $this->repository->findAll(),
        ];
    }
}

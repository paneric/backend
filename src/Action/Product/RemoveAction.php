<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class RemoveAction
{
    protected $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function remove(String $id): void
    {
        $product = $this->manager
            ->getRepository(Product::class)
            ->find((Int) $id);

        if ($product === null) {
            return;
        }

        $this->manager->remove($product);
        $this->manager->flush();
    }
}

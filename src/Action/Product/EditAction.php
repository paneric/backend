<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Form\ProductType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface as FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class EditAction
{
    protected $manager;

    protected $formFactory;

    protected $requestStack;

    public function __construct(EntityManagerInterface $manager, FormFactory $formFactory, RequestStack $requestStack)
    {
        $this->manager = $manager;

        $this->formFactory = $formFactory;

        $this->requestStack = $requestStack;
    }

    public function edit(String $id): ?array
    {
        $request = $this->requestStack->getCurrentRequest();

        $product = $this->manager
            ->getRepository(Product::class)
            ->find((Int) $id);

        if ($request !== null && $request->getMethod() === 'POST') {

            $form = $this->formFactory->create(ProductType::class, $product);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->manager->flush();

                return null;
            }
        }

        $form = $this->formFactory->create(ProductType::class, $product);

        return [
            'form' => $form->createView(),
            'id' => $id,
        ];
    }
}

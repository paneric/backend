<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepository;

class ShowByStateAction
{
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function showByState(Int $state): array
    {
        return $this->repository->findBy(
            ['state' => $state]
        );
    }
}

<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Form\ProductType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface as FormFactory;

use Symfony\Component\HttpFoundation\RequestStack;

class AddAction
{
    protected $manager;

    protected $formFactory;

    protected $requestStack;

    public function __construct(EntityManagerInterface $manager, FormFactory $formFactory, RequestStack $requestStack)
    {
        $this->manager = $manager;

        $this->formFactory = $formFactory;

        $this->requestStack = $requestStack;
    }

    public function add(): ?array
    {
        $product = new Product();

        $request = $this->requestStack->getCurrentRequest();

        if ($request !== null && $request->getMethod() === 'POST') {
            $form = $this->formFactory->create(ProductType::class, $product);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $product = $form->getData();

                $this->manager->persist($product);
                $this->manager->flush();

                return null;
            }
        }

        $form = $this->formFactory->create(ProductType::class, $product);

        return [
            'form' => $form->createView(),
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Person;
use App\Repository\PersonRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Person|Proxy createOne(array $attributes = [])
 * @method static Person[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static Person|Proxy findOrCreate(array $attributes)
 * @method static Person|Proxy random(array $attributes = [])
 * @method static Person|Proxy randomOrCreate(array $attributes = [])
 * @method static Person[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Person[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static PersonRepository|RepositoryProxy repository()
 * @method Person|Proxy create($attributes = [])
 */
final class PersonFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'login' => self::faker()->lexify('??????????'),
            'lName' => self::faker()->lastName,
            'fName' => self::faker()->firstName,
            'state' => self::faker()->numberBetween(1, 3),
        ];
    }

    protected static function getClass(): string
    {
        return Person::class;
    }
}

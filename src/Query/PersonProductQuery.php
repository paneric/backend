<?php

declare(strict_types=1);

namespace App\Query;

use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;

class PersonProductQuery
{
    protected $manager;

    protected $biases;

    public function __construct(EntityManagerInterface $manager, array $biases)
    {
        $this->manager = $manager;

        $this->biases = $biases;
    }

    public function findPersonProductsByBias(Int $personId, Int $bias): array
    {
        try {
            $conn = $this->manager->getConnection();

            $sql = sprintf(
                'SELECT * FROM product pt
                WHERE pt.id %s (
                    SELECT plp.product_id
                    FROM person_like_product plp
                    WHERE plp.person_id = :person_id
                )
                ORDER BY pt.name',
                $this->biases[$bias]
            );

            $stmt = $conn->prepare($sql);
            $stmt->execute(['person_id' => $personId]);

            return $stmt->fetchAllAssociative();
        } catch (Exception | \Doctrine\DBAL\Driver\Exception $e) {
            echo $e->getMessage();
        }

        return [];
    }

    public function findProductsByBias(Int $bias): array
    {
        try {
            $conn = $this->manager->getConnection();

            $sql = sprintf(
                'SELECT * FROM product pt
                WHERE id %s (
                    SELECT DISTINCT product_id FROM person_like_product
                )
                ORDER BY pt.name',
                $this->biases[$bias]
            );

            $stmt = $conn->executeQuery($sql);

            return $stmt->fetchAllAssociative();
        } catch (Exception | \Doctrine\DBAL\Driver\Exception $e) {
            echo $e->getMessage();
        }

        return [];
    }
}

# HONETi application chalenge

Junior PHP Developer test challenge

## Tested environment

* **Linux Ubuntu 18.04.5**
* **PHP 7.3.26**  
* **Symfony 5.21**
* **MySQL 5.7.32**


## Folder structure

* **backend**
    * **config**
    * **migrations**
    * **public**      
    * **src**
        * **Action**
        * **Controller**
        * **DataFixtures**  
        * **Entity**
        * **Extension**
        * **Factory**
        * **Query**  
        * **Repository**
    * **templates**
    * .gitignore  
    * composer.json
    * composer.lock
    * README.md 
    * symfony.lock

## Introductory remarks

### Naming convention

Modules naming convention in principle follows the database schema, so there are, instead of `user` -> `person` and 
`liked` -> `person_product` terms adapted.

### Practices and principles:

#### "Thin controller, fat service"

There has been a "thin controller fat service" practice implemented, slightly modified with single "service action"
approach relation to services methods instead of those that are appropriate to controllers.  
Such approach allows keeping controllers body the thinnest possible and hand all logic over its methods related "service
actions".

#### "Separation of concerns"
By personal preference and according to above principle, routes annotations within a controller are avoided and 
configured in appropriate configuration file even despite commonly applied practice among Symfony developers.  

In case of Doctrine the situation looks a little more complicated, so let it be, especially that in its case annotations
makes things much, much more smooth and handy.

#### "Modular separation"

In my personal opinion all classes related to a module should be gathered in one folder within a project structure,
to keep both, related logic and files together separated from other modules, but following instructions received by 
email there is a common Symfony's developers files arrangement kept.

## Clone git repository

After selecting the target location of the project, run following commands within your system terminal:

```sh
$ git clone https://paneric@bitbucket.org/paneric/backend.git
```
This will clone the project into the folder `/backend` which you're supposed to open in your favorite IDE.

## .env

Just for a testing purpose, there is exceptionally included .env file w predefined environmental variables.

```
APP_ENV=dev
APP_SECRET=6ba5f648ae9a9252910857358d976fe6

DATABASE_URL="mysql://root:root@127.0.0.1:3306/DB_Model?serverVersion=5.7"
```

## Installation

```sh
$ sh bin/install.sh
```

## Messing around with application

### Main page (Persons list)

![Persons](./public/img/persons.png "Persons")

```
http://127.0.0.1:8000
```

Main page, to keep things simple, loads Persons-Users list arranged within a table, with individual links for each 
Person-User, destined for liked products listing - `Products preferences`, Person-User data edition - `Edit`, and finally
Person-User removal - `Remove`.  
Apart from that, there is a self-explanatory button `Add person` included, along with a link `Products` to present 
Products list and four states filters `All`, `Active`, `Banned`, `Removed`.

#### Person Products preferences

![Products liked](./public/img/product_preferences_liked.png "Products liked")

Product preferences page presents a list of products liked by an individual user. Each product can be removed from this 
list with an assistance of `Change preference` link by a simple click.
Similarly to the Persons page, there is a couple of useful links - `Persons` to load Persons-Users list along with
`Ignored Products` useful to mark some ignored product as liked.  

![Products ignored](./public/img/product_preferences_ignored.png "Products ignored")

### Product list

Similar to the list of products.

![Products](./public/img/products.png "Products")
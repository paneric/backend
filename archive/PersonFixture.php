<?php

namespace App\DataFixtures;

use App\Factory\PersonFactory;
use App\Factory\ProductFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PersonFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        PersonFactory::new()
            ->many(20)
            ->create(function() {
                return ['tags' => ProductFactory::randomRange(0, 5)];
            })
        ;

        $manager->flush();
    }
}
